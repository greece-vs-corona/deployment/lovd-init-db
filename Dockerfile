FROM busybox:1

RUN mkdir /scripts
RUN mkdir /dump

COPY /scripts/* /scripts/
COPY /dump/gnomad-common.tar.gz /dump/

CMD tar xvf /dump/gnomad-common.tar.gz --directory /docker-entrypoint-initdb.d && cp /scripts/* /docker-entrypoint-initdb.d
