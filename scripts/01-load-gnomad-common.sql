CREATE TABLE IF NOT EXISTS gnomad_common (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  variants VARCHAR(1000),
  PRIMARY KEY (id),
  INDEX variants_index(variants)
);

LOAD DATA LOCAL INFILE '/docker-entrypoint-initdb.d/gnomad-common.txt' 
  INTO TABLE gnomad_common
  (variants)
;

